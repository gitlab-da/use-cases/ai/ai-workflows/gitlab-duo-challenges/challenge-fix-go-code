# Challenge: Fix Go code

The project provides a Go program that does something.

1. Use GitLab Duo Chat to understand the source code.
1. Try to build the binaries, and run the tests in CI/CD.
    - Learn how to fix source code and the tests using GitLab Duo Chat.
    - Code Suggestions can also help.
1. CI/CD pipelines will fail later, too.
    - Use root cause analysis or GitLab Duo Chat to fix them.

The surprise awaits inside the deploy job log.

Bonus: Change the CI/CD variable `IMAGE_PATH` to the alternative proposed path.

## Tips

1. Use [`/explain`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide) to understand what the code does.
1. Use [`/refactor`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide) with refined prompts.
1. Use [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/) to auto-complete missing code and dependencies.
1. Be creative and combine [Root Cause Analysis](https://docs.gitlab.com/ee/user/ai_features.html#root-cause-analysis) with [Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) and Code Suggestions.
1. Verify that the code is working, locally or by using the provided CI/CD pipeline.

<details>
<summary>If you are stuck</summary>
All areas are marked with `[🦊]`.

1. main.go - imports
1. main.go - tests
1. .gitlab-ci.yml - bonus
</details>

### Program 

Docker:

```sh
docker run -ti -v `pwd`:/src golang:latest bash

cd /src

go build -o tanuki

./tanuki
```

macOS:

```sh
brew install go

go build -o tanuki

./tanuki
```

## Solution

[solution/](solution/).

## Author

@dnsmichi